import { createSelector } from "reselect";

const selectAmbassador = (state: any) => state.ambassador;

export const selectAmbassadorId = createSelector(
  [selectAmbassador],
  (ambassador) => ambassador.ambassadorId
);

export const selectAmbassadorFetching = createSelector(
  [selectAmbassador],
  (ambassador) => ambassador.isAmbassadorFetching
);
