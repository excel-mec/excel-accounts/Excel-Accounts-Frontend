import { SET_AMBASSADOR, SET_AMBASSADOR_FETCHING } from '../constants'
import http from '../../config/http'

export const setAmbassador = (dispatch: any, response: any) => {
  const { ambassadorId = 0, freeMembership = 0, paidMembership = 0 } =
    response || {}

  dispatch({
    type: SET_AMBASSADOR,
    payload: { ambassadorId, freeMembership, paidMembership },
  })
}

export const signUpForAmbassador = (dispatch: any) => {
  http
    .getWithRefresh('/Ambassador/signup')
    .then((response: any) => {
      if (response.response === 'Success') {
        http
          .getWithRefresh('/Ambassador')
          .then((response: any) => {
            // console.log(response);
            setAmbassador(dispatch, response)
          })
          .catch((e) => console.log('Error fetching ambassador details: ', e))
      }
    })
    .catch((e) => console.log('Error fetching ambassador signup: ', e))
}

export const setAmbassadorFetching = (dispatch: any, isFetching: boolean) => {
  dispatch({
    type: SET_AMBASSADOR_FETCHING,
    isFetching,
  })
}
