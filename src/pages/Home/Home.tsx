import React, { useEffect, useState, lazy, Suspense } from "react";

import http from "../../config/http";
import DottedLineLoader from "../../components/common/Loaders/DottedLineLoader";
import "./Home.scss";

const RegEvCard = lazy(() =>
  import("../../components/RegisteredEvents/RegEvCard")
);
const Table = lazy(() => import("../../components/RegisteredEvents/Table"));

interface IBookmark {
  "id": number;
  "name": string;
  "icon": string;
  "eventType": string;
  "category": string;
  "datetime": string;
  "isRegistered": boolean;
  "needRegistration": boolean;
}

interface IRegistration {
  "id": number;
  "name": string;
  "icon": string;
  "eventType": string;
  "category": string;
  "datetime": string;
  "needRegistration": boolean;
  "venue": string;
  "day": number;
}

interface IRegistrationItem {
  no: number;
  name: string;
  dateTime: string;
  link: string;
}
type IBookmarkItem = IRegistrationItem;
// type IRegistrationItem = TableRow;
// type IBookmarkItem = TableRow;

const Home = () => {
  const [registeredEvents, setRegisteredEvents] = useState<IRegistrationItem[]>([])
  const [bookmarkedEvents, setBookmarkedEvents] = useState<IBookmarkItem[]>([]);

  const [isRegistrationFetching, setIsRegistrationFetching] = useState(false);
  const [isBookmarksFetching, setIsBookmarksFetching] = useState(false);
  // const [areResultsFetching, setAreResultsFetching] = useState(false);

  useEffect(() => {
    // http.getWithRefresh("/test").then((res: any) => console.log(res));
    setIsRegistrationFetching(true);
    http.getWithRefresh('/registration', 'EventsApiRoot')
      .then((res: IRegistration[]) => {
        // console.log("registered events: ", res)
        if (res) {
          const data = res.sort((a, b) => a.name > b.name ? 1 : -1).map((item, i: number) => (
            {
              no: i + 1,
              name: item.name,
              dateTime: item.datetime,
              link: ''
              // link: `excelmec.org/events/${item.id}` 
            }
          ))
          setRegisteredEvents(data);
        }
      })
      .catch((e) => console.log("Error fetching registration details: ", e))
      .finally(() => setIsRegistrationFetching(false));
  }, []);
  useEffect(() => {
    setIsBookmarksFetching(true)
    http.getWithRefresh('/bookmark', 'EventsApiRoot')
      .then((res: IBookmark[]) => {
        // console.log("bookmarked events: ", res)
        if (res) {
          const data = res.sort((a, b) => a.name > b.name ? 1 : -1).map((item, i: number) => (
            {
              no: i + 1,
              name: item.name,
              dateTime: item.datetime,
              link: ''
              // link: `excelmec.org/events/${item.id}` 
            }
          ))
          setBookmarkedEvents(data);
        }
      })
      .catch((e) => console.log("Error fetching bookmarks details: ", e))
      .finally(() => setIsBookmarksFetching(false));
  }, []);

  return (
    <div className="content">
      <div className="container-fluid">
        <Suspense fallback={<DottedLineLoader />}>
          <div className="row row-eq-height">
            <RegEvCard
              title={registeredEvents.length}
              desc="Registered Events"
              icon="playlist_add_check"
              icon_bg="warning"
            />
            <RegEvCard
              title={bookmarkedEvents.length}
              desc="Bookmarked Events"
              icon="book"
              icon_bg="danger"
            />
            <RegEvCard
              title={0}
              desc="Results"
              icon="thumb_up"
              icon_bg="primary"
            />
          </div>
        </Suspense>
        <Suspense fallback={<DottedLineLoader />}>
          <div className="row">
            <div className="col-lg-6 col-md-12">
              <Table tableData={registeredEvents} tableTitle="Registered Events" isEmpty={!registeredEvents.length} isFetching={isRegistrationFetching} emptyText='You have not registered for any events yet' />
              <Table tableData={bookmarkedEvents} tableTitle="Bookmarked Events" isEmpty={!bookmarkedEvents.length} isFetching={isBookmarksFetching} emptyText="You haven't bookmarked any events" />
            </div>
            <div className="col-lg-6 col-md-12">
              <Table tableData={[]} tableTitle="Results" isEmpty={true} isFetching={false} emptyText='No results yet' />
            </div>
          </div>
        </Suspense>
      </div>
    </div>
  );
};

export default Home;
