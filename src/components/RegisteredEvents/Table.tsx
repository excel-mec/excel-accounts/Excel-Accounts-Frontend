import React from "react";

export interface TableRow {
  no: number;
  name: string;
  link: string;
  [key: string]: any;
}

interface TableProps {
  tableData: TableRow[];
  tableTitle: string;
  isEmpty: boolean;
  isFetching: boolean;
  emptyText: string;
}

const Table = ({ tableData, tableTitle, isEmpty, isFetching, emptyText }: TableProps) => {
  const tableFields = tableData[0] ? Object.keys(tableData[0]).slice(0, -1) : [];
  return (
    <div className="row" style={{ margin: "5px" }}>
      <div className="card" id={tableTitle}>
        <div className="card-header card-header-primary">
          <h4 className="card-title">{tableTitle}</h4>
          {/* <p className='card-category'>New employees on 15th September, 2016</p> */}
        </div>
        <div className="card-body table-responsive">
          <table className="table table-hover">
            <thead>
              {!!tableFields.length && tableFields.map((field, i) => (
                <th key={i}>
                  {field.charAt(0).toUpperCase() + field.slice(1)}
                </th>
              ))}
            </thead>
            <tbody>
              {!!tableData.length &&
                tableData.map(({ no, name, link, dateTime, ...rest }: TableRow, i: number) => (
                  <tr key={i}>
                    <td>{no}</td>
                    <td>{name}</td>
                    <td>{dateTime}</td>
                    {!!rest && Object.values(rest).map((x) => (<td>{x}</td>))}
                    {!!link && <td className="view-btn">
                      <a
                        href={link}
                        rel="noopener noreferrer"
                        target="_blank"
                      >
                        <button className="btn btn-primary">View</button>
                      </a>
                    </td>}
                  </tr>
                ))}
              {
                isFetching ? <tr><td>Please wait...</td></tr> : isEmpty ? <tr><td>{emptyText}</td></tr> : ''
              }
            </tbody>
          </table>
        </div>
      </div>
    </div>
  );
};

export default Table;
